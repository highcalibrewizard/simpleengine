package com.highcalibrewizard.simpleengine;

import java.awt.image.BufferedImage;
import java.util.List;

public class Animation {
	private List<BufferedImage> images; 
	private int speed;
	private int currentFrame;
	private long lastExecution;
	private boolean isPlaying;
	private boolean play;
	
	/**
	 * Creates an animation container. Animation has a coupling to Entity, since the Rectangle class determines the sinze and location of the
	 * drawn image
	 * @param images list of BufferedImages
	 * @param speed multiple of BASE_REFRESH in Game class. 1 is fastest (~30 FPS), 2 is slower (~15 FPS) etc.
	 */
	public Animation (List<BufferedImage> images, int speed) {
		this.images = images;
		this.speed = speed;
	}
	
	/**
	 * Returns the current framenumber, starting from 0
	 * @return current frame index
	 */
	public int getCurrentFrame() {
		return currentFrame;
	}
	
	/**
	 * Sets play to false, resets the timer, but keeps the current frame, use reset() to start from first frame
	 */
	public void stop() {
		play = false;
		isPlaying = false;
		lastExecution = 0;
	}
	
	/**
	 * Sets play to true, resets the timer, but keeps the current frame, use reset() to start from first frame
	 */
	public void start() {
		play = true;
		isPlaying = true;
		lastExecution = 0;
	}
	
	
	/**
	 * Check if animation is enabled to play.
	 * @return boolean flag if animation should play
	 */
	public boolean ifEnabled() {
		return play;
	}
	
	/**
	 * Resets the current frame to starting position. Does not change any other state
	 */
	public void reset() {
		currentFrame = 0;
	}
	
	/**
	 * Used to determine if there are frames that have not played yet. Use this for animations that should stop after 1 loop 
	 * @return boolean true if animation counter is in middle of loop
	 */
	public boolean isPlaying() {
		return isPlaying;
	}
	
	/**
	 * Convenience method when you want to display a certain frame (for example if you want the animation to stop after isPlaying returns false
	 * but still want to display the first frame)
	 * @return specific image depending on supplied frame index
	 */
	public BufferedImage serveImage(int frameToShow) {
		return images.get(frameToShow);	
	}
	
	/**
	 * Serves the current appropriate image to draw. It uses the internal system timer. It is setup so that it will loop, so 
	 * if you want it to only play once, check if isPlaying() returns false.
	 * @return image depending on current index, supplied via internal counter
	 */
	public BufferedImage serveImage() {
		BufferedImage img =  images.get(currentFrame);	
		long current = System.currentTimeMillis();
		if ((current - lastExecution) >= ((Game.BASE_REFRESH + Game.getLag()) * speed)) {
			if ((currentFrame+1) >= images.size()) {
				isPlaying = false;
				currentFrame = 0;
			} else {
				isPlaying = true;
				currentFrame++;
			}
			lastExecution = current;
		}
		
		return img;
			
	}
}
