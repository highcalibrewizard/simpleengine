package com.highcalibrewizard.simpleengine;

import java.io.File;

import main.java.goxr3plus.javastreamplayer.stream.StreamPlayer;
import main.java.goxr3plus.javastreamplayer.stream.StreamPlayerException;

/**
 * Wrapper for the StreamPlayer class. Use this for convenience to play sound files
 * @author bandersson
 *
 */
public class StreamPlayerWrapper {
	private volatile StreamPlayer player;
	public File file;
	
	/**
	 * Initialize the wrapper for the file you want to play
	 * @param file to load
	 */
	public StreamPlayerWrapper(File file) {
		this.file = file;
		player = new StreamPlayer();
		try {
			player.open(file);
		} catch (StreamPlayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Plays the clip on a separate thread since the base StreamPlayer runs on the main one.
	 */
	public void play() {
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					if (player.isPlaying()) {
						player.stop();
					}
					player.play();
				} catch (StreamPlayerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}).start();
	}
	
}
