package com.highcalibrewizard.simpleengine;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;

public interface Tick {
	/**
	 * The drawing/tick callback. Use the Graphics context to draw using that GameCanvas' drawing methods. You will need
	 * to clear the surface before drawing using the GameCanvas' clear() method
	 * Drawing is double buffered, and releasing the buffer is handled internally.
	 * @param g Graphics context supplied from internally 
	 */
	void tick(Graphics g);
	/**
	 * Returns mouse position if on canvas. The update speed of this callback is dependant of the internal tick rate currently set. It might be
	 * unreliable for high speed and accuracy situations
	 * @param p mouse position Point
	 */
	void mousePosition(Point p);
	/**
	 * Triggers if mouse is clicked
	 * @param e MouseEvent: holds pointer and button information
	 */
	void mouseClick(MouseEvent e);
	/**
	 * Triggers when keyboard button is pressed. Compare key pressed with constants in KeyEvent
	 * @param key that is pressed
	 */
	void keyPressed(int key);
}
