package com.highcalibrewizard.simpleengine;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Use this class's static method getImage to load images
 * @author bandersson
 *
 */
public class ImageLoader {

	/**
	 * Loading in images. Put them in root directory.
	 * @param file the filename of image you want to load
	 * @return BufferedImage or null
	 */
	public static BufferedImage getImage(String file) {
		BufferedImage img = null;
		
		try {
			img = ImageIO.read(new File(file));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		return img;
	}
	
	/**
	 * Convenience method for grabbing a number of images as a list, for Animation class for example
	 * @param file Multiple string names of file/URI
	 * @return List of all loaded BufferedImages
	 */
	public static List<BufferedImage> getImagesAsList(String... file) {
		List<BufferedImage> list = new ArrayList<>();
		for (String f : file) {
			BufferedImage img = null;
			
			try {
				img = ImageIO.read(new File(f));
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			
			list.add(img);
		}
		return list;
	}
}
