package com.highcalibrewizard.simpleengine;

import java.awt.Font;

public class CanvasText {
	private String string;
	private Font font;
	private int x;
	private int y;
	
	/**
	 * Getting current x-pos
	 * @return x-pos
	 */
	public int getX() {
		return x;
	}

	/**
	 * Setting x-pos
	 * @param x x-position
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Getting current y pos
	 * @return y-position
	 */
	public int getY() {
		return y;
	}
	/**
	 * Setting y-pos
	 * @param y y-position
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Getting stored current font
	 * @return current font
	 */
	public Font getFont() {
		return font;
	}

	/**
	 * Setting the font manually
	 * @param font the Font
	 */
	public void setFont(Font font) {
		this.font = font;
	}
	
	/**
	 * Constructing canvastext object for drawing
	 * @param string text
	 * @param x x-pos
	 * @param y y-pos
	 * @param font selected font
	 */
	public CanvasText(String string, int x, int y, Font font) {
		this.string = string;
		this.x = x;
		this.y = y;
		this.font = font;
	}
	
	/**
	 * Setting text
	 * @param string your text
	 */
	public void setText(String string) {
		this.string = string;
	}
	/**
	 * returning stored text
	 * @return text
	 */
	public String getText() {
		return string;
	}
	
	
}
