package com.highcalibrewizard.simpleengine;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;

/**
 * Helper class for loading fonts.
 * @author bandersson
 *
 */
public class FontLoader {
	
	/**
	 * Creates a custom font from file, with size and style. Will return null if errors
	 * Only works with TrueType (ttf)
	 * @return Font the font you tried to load, or null
	 */
	public static Font loadFont(File file, int size, int style) {
		Font newFont = null;
		try {
			newFont = Font.createFont(Font.TRUETYPE_FONT, file);
			newFont = newFont.deriveFont(style, size);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return newFont;
	}
}
