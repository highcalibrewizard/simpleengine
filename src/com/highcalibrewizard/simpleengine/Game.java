package com.highcalibrewizard.simpleengine;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;
import javax.swing.Timer;

/**
 * Base class for the game engine.
 * The GUI is based of the Swing framework.
 * Project head: Benny Andersson highcalibrewizard@gmail.com 
 * @author bandersson
 *
 */
public final class Game {
	
	private volatile Tick tickCallback;
	private JFrame frame;
	private GameCanvas canvas;
	private boolean run = true;
	private MouseCanvas mouseListener;
	private static long lag = 0;
	/**
	 * The refresh rate for the internal ticker. The anticipated framerate is 1000/33 = 30 
	 */
	protected static int BASE_REFRESH = 33;
	
	/**
	 * Entry point for SimpleEngine framework
	 * @param dimensions size of window (actually the drawing area)
	 * @param title window title
	 * @param tickCallback implemented Tick interface in calling class
	 * @param bgColor background color of window
	 * @param fullScreen if game should be displayed full screen (no decorations) or not. If decorated canvas drawing area does not start at 0,0
	 */
	public Game(Dimension dimensions, String title, Tick tickCallback, Color bgColor, boolean fullScreen) {
		frame = new JFrame(title);
		frame.setSize(dimensions);
		frame.setResizable(false);
		if (fullScreen) {
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
			frame.setUndecorated(true);
		}
		//frame.setBackground(bgColor);
		canvas = new GameCanvas(dimensions, bgColor);
		frame.add(canvas);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.tickCallback = tickCallback;
	}
	
	/**
	 * Sets the refresh rate in milliseconds (used to calculate number of updates per second). By default this is set to 33
	 * @param refreshRate 
	 */
	public void setRefreshRate(int refreshRate) {
		BASE_REFRESH = refreshRate;
	}
	
	/**
	 * Returns the default or currently set refresh rate
	 * @return the refresh rate in milliseconds
	 */
	public int getRefreshRate() {
		return BASE_REFRESH;
	}
	
	/**
	 * Used to switch context, or in other words: level or scene.
	 * @param ticker implementation of the Tick interface
	 */
	public void setContext(Tick ticker) {
		this.tickCallback = ticker;
		mouseListener.setTicker(ticker);
	}
	
	/**
	 * returns the time of execution
	 * @return time of execution in milliseconds
	 */
	public static long getLag() {
		return lag;
	}
	
	/**
	 * Gets approximate framerate
	 * @return number of frames/second taking time of execution and the sleep counter into consideration
	 */
	public static long getFPS() {
		return 1000/(BASE_REFRESH-Game.getLag());
	}
	
	/*
	 * Starts ticking, need to call this manually AFTER you have loaded all resources and created the GameArea to prevent loop from locking 
	 * up other initializtions
	 */
	public void init() {
		frame.setVisible(true);
		canvas.requestFocus();
		canvas.createBufferStrategy(2);
		mouseListener = new MouseCanvas(tickCallback);
		canvas.addMouseListener(mouseListener);
		canvas.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				tickCallback.keyPressed(arg0.getKeyCode());
				
			}
		});
		
		startTick();
	}
	
	private void startTick() {

		Timer timer = new Timer(BASE_REFRESH, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				long then = System.currentTimeMillis();
				mouseListener.propagateMousePosition();
				BufferStrategy strat = canvas.getBufferStrategy();
				Graphics g = strat.getDrawGraphics();
				tickCallback.tick(g);
				g.dispose();
				strat.show();
				long now = System.currentTimeMillis();
				lag = then - now;
				
			}
		});
		timer.setRepeats(true);
		timer.setInitialDelay(0);
		timer.start();
	}

	/**
	 * Returns the canvas (the drawing area)
	 * @return GameCanvas
	 */
	public GameCanvas getCanvas() {
		return canvas;
	}
	
}
