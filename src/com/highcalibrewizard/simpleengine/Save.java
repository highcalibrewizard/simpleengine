package com.highcalibrewizard.simpleengine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.google.gson.Gson;

public class Save {
	private static Save save = new Save();
	Gson gson;
	
	private Save() {
		gson = new Gson();
	}
	
	/**
	 * Use this to get the Save instance
	 * @return instance of Save
	 */
	public  static Save getInstance() {
		return save;
	}
	
	/**
	 * Saves object to file. Using gson
	 * @param file to save to
	 * @param o object to save
	 * @return boolean if save succeeded
	 */
	public boolean toJson(File file, Object o) {
		String json = gson.toJson(o);
		
		FileOutputStream out;
		OutputStreamWriter outS;
		BufferedWriter writer;
		
		try {
			out = new FileOutputStream(file);
			outS = new OutputStreamWriter(out, "UTF8");
			writer = new BufferedWriter(outS);
			writer.write(json);
			writer.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Loads object from file. Uses gson. Cast return value to prefered object
	 * @param classType Class type to convert to
	 * @param file to load from
	 * @return loaded object Object, or null
	 */
	public <T> Object fromJson(File file, Class<T> classType) {
		FileInputStream stream;
		InputStreamReader reader;
		BufferedReader bufferedReader;
		StringBuilder builder = new StringBuilder();
		
		try {
			stream = new FileInputStream(file);
			reader = new InputStreamReader(stream);
			bufferedReader = new BufferedReader(reader);
			int c;
			while ((c = bufferedReader.read()) != -1) {
				builder.append((char)c);
			}
			bufferedReader.close();
			return gson.fromJson(builder.toString(), classType);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}
}
