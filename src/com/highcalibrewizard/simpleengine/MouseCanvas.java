package com.highcalibrewizard.simpleengine;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public final class MouseCanvas implements MouseMotionListener, MouseListener {

	private boolean onCanvas = false;
	private Tick ticker;
	private static Point mousePosition;

	protected MouseCanvas(Tick ticker) {
		this.ticker = ticker;
	}
	
	protected void setTicker(Tick ticker) {
		this.ticker = ticker;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		ticker.mouseClick(e);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		onCanvas = true;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		onCanvas = false;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Returns current mouse position. The update speed of this callback is dependant of the internal tick rate currently set. It might be
	 * unreliable for high speed and accuracy situations
	 * @return mousePosition or null if pointer not on canvas
	 */
	public static Point getMousePoint() {
		return mousePosition;
	}

	/**
	 * Used to call the Tick interface. Please use getMousePoint() to get current pointer location
	 */
	protected void propagateMousePosition() {
		if (onCanvas) {
			ticker.mousePosition(MouseInfo.getPointerInfo().getLocation());
			mousePosition = MouseInfo.getPointerInfo().getLocation();
		}
		else {
			mousePosition = null;
		}
		
	}

}
