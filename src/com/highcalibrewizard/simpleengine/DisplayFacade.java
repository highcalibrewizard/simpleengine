package com.highcalibrewizard.simpleengine;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
/**
 * Convenience class for grabbing scaling and screen size
 * @author bandersson
 *
 */
public class DisplayFacade {
	
	private static DisplayFacade facade = new DisplayFacade();
	private int height;
	private int width;
	/**
	 * Private constructor. Use getInstance()
	 */
	private DisplayFacade() {
		GraphicsEnvironment en = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = en.getDefaultScreenDevice();
		DisplayMode mode = device.getDisplayMode();
		height = mode.getHeight();
		width = mode.getWidth();
	}
	
	/**
	 * Getting the DisplayFacade instance
	 * @return DisplayFacade instance
	 */
	public static DisplayFacade getInstance() {
		return DisplayFacade.facade;
	}
	
	
	/**
	 * Getting screen resolution height
	 * @return height
	 */
	public int getHeight() {
		return height;
	}
	/**
	 * Getting screen resolution width
	 * @return width
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Used for calculate pixel scaling, to make sure images are displayed with same ratios on different screens.
	 * It calculates difference between current screen size and supplied virtual screen size.
	 * Use it by supplying a virtual screen size (preferably your own development screen)
	 * It is calculated as factual screen width / virtual width.
	 * @param virtualWidth the width in pixels
	 * @return current scaling horizontally
	 */
	public float calculateScalingX(float virtualWidth) {
		return width/virtualWidth;
	}
	
	/**
	 * Used for calculate pixel scaling, to make sure images are displayed with same ratios on different screens.
	 * It calculates difference between current screen size and supplied virtual screen size.
	 * Use it by supplying a virtual screen size (preferably your own development screen)
	 * It is calculated as factual screen height / virtual height.
	 * @param virtualHeight height in pixels
	 * @return current scaling vertically
	 */
	public float calculateScalingY(float virtualHeight) {
		return height/virtualHeight;
	}
	
	/**
	 * Convenience method to scale an integer depending on screen. This is for width
	 * @param inputX x size to scale
	 * @param virtualWidth your prefered virtual width
	 * @return scaled x size
	 */
	public int getScaledX(int inputX, float virtualWidth) {
		return (int)(inputX*calculateScalingX(virtualWidth));
	}
	
	/**
	 * Convenience method to scale an integer depending on screen. This is for height
	 * @param inputY
	 * @param virtualHeight
	 * @return scaled y size
	 */
	public int getScaledY(int inputY, float virtualHeight) {
		return (int)(inputY*calculateScalingY(virtualHeight));
	}
	

}
