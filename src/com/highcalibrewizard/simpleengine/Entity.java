package com.highcalibrewizard.simpleengine;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
/**
 * The base class for objects on screen, holding hitbox Rectangle. Extend this class for further functionality,
 * like health points or similar. Serialization of these objects may not be possible straight up becuase of the BufferedImage
 * and Animation object dependencies. It is recommended you use a custom object to store values you want to save, and then apply them
 * to Entities.
 * @author bandersson
 *
 */
public class Entity {
	private Rectangle hitBox;
	private BufferedImage image;
	private int xSpeed;
	private int ySpeed;
	private int speed;
	private Animation animation;
	
	/**
	 * Simple method for checking collisions
	 * @param otherHitbox another Rectangle
	 * @return boolean true if overlap
	 */
	public boolean detectCollision(Rectangle otherHitbox) {
		try {
			return hitBox.intersects(otherHitbox);
		} catch (NullPointerException e) {
			e.getStackTrace();
		}
		return false;
	}
	
	/**
	 * Detecting collision with for example mouse pointer and the Entities hitbox Rectangle
	 * @param p the Point to check for
	 * @return true if collision
	 */
	public boolean detectCollision(Point p) {
		try {
			return hitBox.contains(p);
		} catch (NullPointerException e) {
			e.getStackTrace();
		}
		return false;
		
	}
	
	/**
	 * Setting general speed
	 * @param speed Should be positive integer. Reverse with -getSpeed 
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	/**
	 * Getting current general speed
	 * @return speed
	 */
	public int getSpeed() {
		return speed;
	}
	
	/**
	 * Getting current x-speed. Can be negative, compared to general speed
	 * @return x-speed
	 */
	public int getXSpeed() {
		return xSpeed;
	}
	
	/**
	 * Getting current y-speed. Can be negative, compared to general speed
	 * @return y-speed
	 */
	public int getYSpeed() {
		return ySpeed;
	}
	
	/**
	 * Setting x speed
	 * @param xSpeed speed on x-axis
	 */
	public void setXSpeed(int xSpeed) {
		this.xSpeed = xSpeed;
	}
	
	/**
	 * Setting y speed
	 * @param ySpeed speed on y-axis
	 */
	public void setYSpeed(int ySpeed) {
		this.ySpeed = ySpeed;
	}
	
	/**
	 * Creating new entity object
	 * @param hitBox the supplied hitbox with size and position
	 * @param img the BufferedImage if one should exist, otherwise null
	 */
	public Entity(Rectangle hitBox, BufferedImage img) {
		this.hitBox = hitBox;
		this.image = img;
	}
	
	/**
	 * If animation should be used, set it here after creating entity.
	 * @param animation the Animation
	 */
	public void setAnimation(Animation animation) {
		this.animation =  animation;
	}
	
	/**
	 * Returning current Animation
	 * @return the Animation object
	 */
	public Animation getAnimation() {
		return animation;
	}
	
	/**
	 * Returns the image
	 * @return the BufferedImage associated with the Entity
	 */
	public BufferedImage getImg() {
		return image;
	}
	
	/**
	 * Returns the "hitbox" or Rectangle
	 * @return hitbox
	 */
	public Rectangle getRect() {
		return hitBox;
	}

	/**
	 * Moves object by x and ySpeed attribs
	 */
	public void move() {
		hitBox.x = xSpeed + hitBox.x;
		hitBox.y = ySpeed + hitBox.y;
	}
	
}
