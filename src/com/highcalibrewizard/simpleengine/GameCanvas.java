package com.highcalibrewizard.simpleengine;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public final class GameCanvas extends Canvas {
	private static final long serialVersionUID = 1L;
	private int w;
	private int h;
	private Color bgColor;
	
	protected GameCanvas(Dimension dimension, Color color) {
		this.w = dimension.width;
		this.h = dimension.height;
		this.bgColor = color;
		setBackground(color);
	}
	
	/**
	 * Call this before starting draw
	 * @param g Graphics context supplied by tick()
	 */
	public void clear(Graphics g) {
		g.clearRect(0, 0, w, h);
	}
	
	/**
	 * Draws Entity
	 * @param e Entity to draw
	 * @param g Graphics context supplied by tick()
	 */
	public void drawEntity(Entity e, Graphics g) {
		BufferedImage img = null;
		if ((img = e.getImg()) != null) {
			int x = e.getRect().x;
			int y = e.getRect().y;
			int w = e.getRect().width;
			int h = e.getRect().height;
			
			g.drawImage(img, x, y, w, h, null);
		}
	}
	
	/**
	 * Convenience method for drawing text
	 * @param t CanvasText to draw
	 * @param g Graphics context supplied by tick()
	 */
	public void drawText(CanvasText t, Graphics g) {
		g.setFont(t.getFont());
		g.drawString(t.getText(), t.getX(), t.getY());
	}

	/**
	 * Use this method to draw image frames from animations
	 * @param serveImage BufferedImage grabbed from Animation serveImage()
	 * @param e Entity. The Rectangle object held by Entity is what supplies size and position
	 * @param g Graphics context supplied by tick()
	 */
	public void drawAnimationImage(BufferedImage serveImage, Entity e, Graphics g) {
		if (serveImage != null) {
			int x = e.getRect().x;
			int y = e.getRect().y;
			int w = e.getRect().width;
			int h = e.getRect().height;
			
			g.drawImage(serveImage, x, y, w, h, null);
		}
		
	}
}
